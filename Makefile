run_local:
	go run main.go

run_migrate:
	go run ./db/migrate/migrate.go

run_migrate_down:
	go run ./db/migrate/migrate_down.go

run_docker:
	docker stop gamecurrencyapiserver || true && docker rm gamecurrencyapiserver || true
	docker build --tag gamecurrency-api:local .
	docker run --name gamecurrencyapiserver -d -p 3000:3000 gamecurrency-api:local
