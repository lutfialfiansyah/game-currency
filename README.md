# Game Currency

## Run
 Before run please create the database in postgresql with database name :
```bash
game_currency
```
 After create database run migration :
```bash
make run_migrate
```
copy env.template and rename it to .env
after that adjust your database environment
```bash
make run_local
```
or
```bash
make run_docker
```

## Documentation
- Documentation REST API & Database Structure : 
```bash
https://docs.google.com/document/d/1IOWibdkn92Qm5ziro-UvKkoHr8k8kFrjmaGwpu_myjY/edit?usp=sharing
```
- Collection POSTMAN :
```bash
https://www.getpostman.com/collections/3209ac81a758b54efae5
```

