package game_currency

import (
	"game-currency/domain/game_currency"
	"game-currency/domain/game_currency/model"
	"github.com/asaskevich/govalidator"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"

	"game-currency/domain/game_currency/repository"
	"game-currency/lib/response"
)

type Controller struct {
	GameCurrencyService game_currency.ServiceInterface
}

func GameCurrencyController(db *gorm.DB) *Controller {
	return &Controller{
		GameCurrencyService: game_currency.NewService(repository.NewRepository(db)),
	}
}

func (av *Controller) GetCurrencies(context *gin.Context) {
	resBody, errStatus, err := av.GameCurrencyService.GetCurrencies()
	if err != nil {
		response.Error(context, errStatus, err.Error())
		return
	}

	response.Json(context, http.StatusOK, resBody)
}

func (av *Controller) AddCurrency(ctx *gin.Context) {
	var req model.RequestCurrency
	if err := ctx.Bind(&req); err != nil {
		response.Error(ctx, http.StatusBadRequest, err.Error())
		return
	}

	// REQUEST VALIDATION
	valid, err := govalidator.ValidateStruct(req)
	if err != nil {
		println("error: " + err.Error())
		response.Error(ctx, http.StatusBadRequest, "Invalid Input : "+err.Error())
		return
	}

	if !valid {
		response.Error(ctx, http.StatusBadRequest, "Invalid Input : "+err.Error())
		return
	}
	data, errStatus, err := av.GameCurrencyService.AddCurrency(req)
	if err != nil {
		response.Error(ctx, errStatus, err.Error())
		return
	}
	response.Json(ctx, http.StatusOK, data)
}

func (av *Controller) AddCurrencyRate(ctx *gin.Context) {
	var req model.RequestCurrencyRate
	if err := ctx.Bind(&req); err != nil {
		response.Error(ctx, http.StatusBadRequest, err.Error())
		return
	}

	// REQUEST VALIDATION
	valid, err := govalidator.ValidateStruct(req)
	if err != nil {
		println("error: " + err.Error())
		response.Error(ctx, http.StatusBadRequest, "Invalid Input : "+err.Error())
		return
	}

	if !valid {
		response.Error(ctx, http.StatusBadRequest, "Invalid Input : "+err.Error())
		return
	}
	data, errStatus, err := av.GameCurrencyService.AddCurrencyRate(req)
	if err != nil {
		response.Error(ctx, errStatus, err.Error())
		return
	}
	response.Json(ctx, http.StatusOK, data)
}


func (av *Controller) CurrencyConvert(ctx *gin.Context) {
	var req model.CurrencyConvert
	if err := ctx.Bind(&req); err != nil {
		response.Error(ctx, http.StatusBadRequest, err.Error())
		return
	}

	// REQUEST VALIDATION
	valid, err := govalidator.ValidateStruct(req)
	if err != nil {
		println("error: " + err.Error())
		response.Error(ctx, http.StatusBadRequest, "Invalid Input : "+err.Error())
		return
	}

	if !valid {
		response.Error(ctx, http.StatusBadRequest, "Invalid Input : "+err.Error())
		return
	}
	data, errStatus, err := av.GameCurrencyService.CurrencyConvert(req)
	if err != nil {
		response.Error(ctx, errStatus, err.Error())
		return
	}
	response.Json(ctx, http.StatusOK, data)
}