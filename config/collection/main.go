package collection

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"game-currency/app/controllers/game_currency"
)

func MainRouter(db *gorm.DB, main *gin.RouterGroup) {
	GameCurrencyCtrl := game_currency.GameCurrencyController(db)
	main.GET("/currency", GameCurrencyCtrl.GetCurrencies)
	main.POST("/currency", GameCurrencyCtrl.AddCurrency)
	main.POST("/currency/rate", GameCurrencyCtrl.AddCurrencyRate)
	main.POST("/currency/convert", GameCurrencyCtrl.CurrencyConvert)
}