package config

import (
	"game-currency/app/controllers/root"
	"game-currency/config/collection"
	"game-currency/db"
	"github.com/gin-gonic/gin"
)

var Routers = gin.Default()

func init() {
	corsConfig(Routers)
	Routers.Static("/assets", "./assets")

	Routers.GET("/", root.Index)
	main := Routers.Group("v1")
	collection.MainRouter(db.DB, main)
}
