-- Table Definition ----------------------------------------------

CREATE TABLE currencies
(
    id   SERIAL PRIMARY KEY,
    name varchar(50) DEFAULT NULL
);

-- Indices -------------------------------------------------------
