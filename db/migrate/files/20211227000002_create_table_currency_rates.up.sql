-- Table Definition ----------------------------------------------

CREATE TABLE currency_rates
(
    id               SERIAL PRIMARY KEY,
    currency_id_from int,
    currency_id_to   int,
    rate             double precision
);

-- Indices -------------------------------------------------------
