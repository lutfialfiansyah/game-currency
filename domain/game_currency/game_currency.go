package game_currency

import (
	"errors"
	"game-currency/domain/game_currency/model"
	"game-currency/domain/game_currency/repository"
	"game-currency/lib/constant"
	"net/http"
)

type ServiceInterface interface {
	GetCurrencies() (*[]model.Currency, int, error)
	AddCurrency(currency model.RequestCurrency) (model.Currency, int, error)
	AddCurrencyRate(currency model.RequestCurrencyRate) (model.CurrencyRate, int, error)
	CurrencyConvert(currency model.CurrencyConvert) (model.CurrencyConvert, int, error)
}

type service struct {
	Repository repository.RepositoryInterface
}

func NewService(repository repository.RepositoryInterface) ServiceInterface {
	return &service{
		Repository: repository,
	}
}

func (s *service) GetCurrencies() (*[]model.Currency, int, error) {
	appVersion, err := s.Repository.GetCurrencies()
	if err != nil {
		return &appVersion, http.StatusInternalServerError, err
	}

	return &appVersion, http.StatusOK, nil
}

func (s *service) AddCurrency(req model.RequestCurrency) (model.Currency, int, error) {
	var store model.Currency
	getByName, err := s.Repository.GetCurrencyByName(req.Name)
	if err != nil {
		return store, http.StatusInternalServerError, err
	}
	if getByName.ID != 0 {
		return store, http.StatusBadRequest, errors.New(constant.AlreadyExists)
	}

	data, err := s.Repository.StoreCurrency(req)
	if err != nil {
		return store, http.StatusInternalServerError, err
	}

	return data, http.StatusOK, nil
}

func (s *service) AddCurrencyRate(req model.RequestCurrencyRate) (model.CurrencyRate, int, error) {
	var store model.CurrencyRate
	getByCurrencyRate, err := s.Repository.GetCurrencyRateByCurrencyFromToID(req)
	if err != nil {
		return store, http.StatusInternalServerError, err
	}
	if getByCurrencyRate.ID != 0 {
		return store, http.StatusBadRequest, errors.New(constant.AlreadyExists)
	}

	data, err := s.Repository.StoreCurrencyRate(req)
	if err != nil {
		return store, http.StatusInternalServerError, err
	}

	return data, http.StatusOK, nil
}

func (s *service) CurrencyConvert(data model.CurrencyConvert) (model.CurrencyConvert, int, error) {
	// MAPPING REQ CURRENCY RATE
	reqRate := model.RequestCurrencyRate{
		CurrencyIDFrom: data.CurrencyIDFrom,
		CurrencyIDTO:   data.CurrencyIDTO,
	}
	getByCurrencyRate, err := s.Repository.GetCurrencyRateByCurrencyFromToID(reqRate)
	if err != nil {
		return data, http.StatusInternalServerError, err
	}
	totalResult := data.Amount / getByCurrencyRate.Rate
	data.ID = getByCurrencyRate.ID
	data.Rate = getByCurrencyRate.Rate
	data.Result = totalResult

	return data, http.StatusOK, nil
}