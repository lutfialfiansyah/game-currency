package model

type Currency struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type RequestCurrency struct {
	ID   int64  `json:"id"`
	Name string `json:"name" form:"name" valid:"required"`
}

type CurrencyRate struct {
	ID             int64   `json:"id"`
	CurrencyIDFrom int64   `json:"currency_id_from"`
	CurrencyIDTO   int64   `json:"currency_id_to"`
	Rate           float64 `json:"rate"`
}

type RequestCurrencyRate struct {
	ID             int64   `json:"id"`
	CurrencyIDFrom int64   `json:"currency_id_from" form:"currency_id_from" valid:"required"`
	CurrencyIDTO   int64   `json:"currency_id_to" form:"currency_id_to" valid:"required"`
	Rate           float64 `json:"rate" form:"rate" valid:"required"`
}

type CurrencyConvert struct {
	ID             int64   `json:"id"`
	CurrencyIDFrom int64   `json:"currency_id_from" form:"currency_id_from" valid:"required"`
	CurrencyIDTO   int64   `json:"currency_id_to" form:"currency_id_to" valid:"required"`
	Rate           float64 `json:"rate"`
	Amount         float64 `json:"amount" form:"amount" valid:"required"`
	Result         float64 `json:"result"`
}
