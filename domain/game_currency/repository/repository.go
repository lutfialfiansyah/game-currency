package repository

import (
	"errors"
	"game-currency/domain/game_currency/model"
	"gorm.io/gorm"
)

type RepositoryInterface interface {
	GetCurrencies() ([]model.Currency, error)
	GetCurrencyByID(id int64) (model.Currency, error)
	GetCurrencyByName(name string) (model.Currency, error)
	GetCurrencyRateByCurrencyFromToID(req model.RequestCurrencyRate) (model.CurrencyRate, error)
	StoreCurrency(req model.RequestCurrency) (model.Currency, error)
	StoreCurrencyRate(req model.RequestCurrencyRate) (model.CurrencyRate, error)
}

type repository struct {
	DB *gorm.DB
}

func NewRepository(DB *gorm.DB) RepositoryInterface {
	return &repository{
		DB: DB,
	}
}

func (r *repository) GetCurrencies() ([]model.Currency, error) {
	var currencies []model.Currency
	if err := r.DB.Table("currencies").
		Select("*").
		Find(&currencies).Error; err != nil {
		return currencies, err
	}

	return currencies, nil
}

func (r *repository) GetCurrencyByID(id int64) (model.Currency, error) {
	var currencies model.Currency
	if err := r.DB.Table("currencies").
		Select("*").
		Where("id = ?", id).
		Find(&currencies).Error; err != nil {
		return currencies, err
	}

	return currencies, nil
}

func (r *repository) GetCurrencyByName(name string) (model.Currency, error) {
	var currencies model.Currency
	if err := r.DB.Table("currencies").
		Select("*").
		Where("name iLIKE ?", `%`+name+`%`).
		Find(&currencies).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return currencies, nil
		}
		return currencies, err
	}

	return currencies, nil
}

func (r *repository) GetCurrencyRateByID(id int64) (model.CurrencyRate, error) {
	var currencies model.CurrencyRate
	if err := r.DB.Table("currency_rates").
		Select("*").
		Where("id = ?", id).
		Find(&currencies).Error; err != nil {
		return currencies, err
	}

	return currencies, nil
}

func (r *repository) GetCurrencyRateByCurrencyFromToID(req model.RequestCurrencyRate) (model.CurrencyRate, error) {
	var currencies model.CurrencyRate
	if err := r.DB.Table("currency_rates").
		Select("*").
		Where("currency_id_from = ? AND currency_id_to = ? " +
			"OR (currency_id_from = ? AND currency_id_to = ?)",
			req.CurrencyIDFrom, req.CurrencyIDTO, req.CurrencyIDTO, req.CurrencyIDFrom).
		Find(&currencies).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return currencies, nil
		}
		return currencies, err
	}

	return currencies, nil
}

func (r *repository) StoreCurrency(req model.RequestCurrency) (model.Currency, error) {
	var currencies model.Currency
	if err := r.DB.Table("currencies").
		Create(&req).Error; err != nil {
		return currencies, err
	}

	// CALLBACK
	currencies, err := r.GetCurrencyByID(req.ID)
	if err != nil {
		return currencies, err
	}

	return currencies, nil
}

func (r *repository) StoreCurrencyRate(req model.RequestCurrencyRate) (model.CurrencyRate, error) {
	var currencies model.CurrencyRate
	if err := r.DB.Table("currency_rates").
		Create(&req).Error; err != nil {
		return currencies, err
	}

	// CALLBACK
	currencies, err := r.GetCurrencyRateByID(req.ID)
	if err != nil {
		return currencies, err
	}

	return currencies, nil
}