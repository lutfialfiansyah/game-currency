module game-currency

go 1.15

require (
	cloud.google.com/go/storage v1.10.0
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/NaySoftware/go-fcm v0.0.0-20190516140123-808e978ddcd2
	github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-resty/resty/v2 v2.6.0
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/joho/godotenv v1.3.0
	github.com/robfig/cron/v3 v3.0.1
	github.com/stretchr/testify v1.5.1
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
	google.golang.org/api v0.30.0
	gorm.io/driver/postgres v1.0.5
	gorm.io/gorm v1.20.5
)
