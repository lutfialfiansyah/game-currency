package main

import (
	"log"

	"game-currency/config"
	_ "github.com/joho/godotenv/autoload"
)

func main() {
	err := config.Routers.Run()
	if err != nil {
		log.Fatal(err)
	}
}
